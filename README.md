# usim
minimal discrete time simulation in javascript

## API

Get the `uSim` class by `var uSim = require("usim");` (node)
or `define(["usim", ...], function (uSim, ...) { /*module code*/}` (AMD).
If needed, you can also just include it in `<script src=".../usim.js"></script>`
and use the global `uSim`.

 - `var usim = new uSim()`
 Creates new simulation timeline with initial time set to `-Infinity`.
 
 - `usim.schedule(time, object)`
 Schedules `object` to occur at `time`.
 
 - `usim.time`
 The current  simulation time.

 - `usim.peek()`
 Returns the object that is scheduled to earliest time or `null`.
 If the object's time is higher than current time,
 the current time is shifted accordingly.

 - `usim.peek(cb)`
 Same as `usim.peek()`, but instead of returning the object,
 it calls the callback with the object or `null` as the only argument.

 - `usim.peekUntil(time)`
 Same as `usim.peek()`, but if time of object if bigger than `time`,
 `null` is returned instead and object stays in the schedule.

 - `usim.peekUntil(time, cb)`
 Same as `usim.peek(cb)`, but if time of object if bigger than `time`,
 `null` is passed instead and object stays in the schedule.

 - `usim.stream(cb)`
 Calls `cb` with all scheduled objects, as if `usim.peek(cb)`
 was called in loop while there are some scheduled objects.

 - `usim.streamUntil(time, cb)`
 Same as `usim.stream(cb)`, but quits if time would be shifted
 past `time` (IOW: streams all objects scheduled up to `time`).

 - 	`usim.run()`
 Same as `usim.stream(function (each) { each(usim) })`.
 IOW, treats all scheduled objects as functions
 and calls them, passing itself as the parameter.
